# X11 Forwarding


## Te voy a enseña como conectarte por ssh y a Redirigir las X11
### Necesitaras
+ Linux (Servidor)
+ Linux/Windows 
 + En caso del linux es un comando
 + En Windows necessitaras **PUTTY + XMING**

---
## TEORIA X11
+ Que necessito para poder ejecutar gparted desde ssh a un servidor:
 + El Servidor **NO** necessita tener instalado las X11 ni ningun escritorio
 + EL que se conecta Necessita X11 y un Escritorio (gnome, xfce, pantheon ...)
 + El servidor necessita de gparted
 
+ Como funciona:

**Servidor** | **Maquina desde la que me conecto** |
--- | --- |
CPU |Grafica |
RAM | |
Disco Duro | |

 + De esta forma el servido ejecuta todo lo pesado y nuestra maquina simplemente dibuja con sus X11


---
### Configuración de Host:
Cada vez que inicie el equipo activa ssh:
 > $ systemctl enable sshd

Arranca ssh ahora
 > $ systemctl start sshd
 
---
### Linux:
Para Conectarnos por SSH usaremos los comando:
 > $ ssh NombreUsuario@IP
 
Para Redirigir las X11 usaremos este otro comando:

 > $ ssh -X NombreUsuario@IP 

---

### Windows:
 + PuTTy se puede descargar [**AQUI**](http://www.chiark.greenend.org.uk/~sgtatham/putty/)
 + Xming se puede descargar [**AQUI**](http://sourceforge.net/project/downloading.php?group_id=156984&filename=Xming-6-9-0-31-setup.exe)

#### Configurando Xming:

---
![](http://www.geo.mtu.edu/geoschem/docs/images/xming_1.jpg)
![](http://www.geo.mtu.edu/geoschem/docs/images/xming_2.jpg)
![](http://www.geo.mtu.edu/geoschem/docs/images/xming_3.jpg)
![](http://www.geo.mtu.edu/geoschem/docs/images/xming_4.jpg)

Guarda la configuración.

---
#### Configurando PUTTY:
![](http://www.geo.mtu.edu/geoschem/docs/images/putty_1.jpg)
![](http://www.geo.mtu.edu/geoschem/docs/images/putty_2.jpg)
![](http://www.geo.mtu.edu/geoschem/docs/images/putty_3.jpg)
![](http://www.geo.mtu.edu/geoschem/docs/images/putty_4.jpg)


---